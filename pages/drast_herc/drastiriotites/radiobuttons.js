/**************************************************************/
/*	         Drasthriothtes Controller                        */
/**************************************************************/
function goDrasthriothta(noumeraki)
{
drastMax=27;
drastMin=1;
drastURL=window.location.toString();
drastURL=drastURL.substr(drastURL.lastIndexOf("dr")+2);
drastURL=drastURL.substr(0,drastURL.indexOf("."));
drastCurrent=parseInt(drastURL);
if (noumeraki=="start") noumeraki=drastMin;
else if (noumeraki=="end") noumeraki=drastMax;
else noumeraki=drastCurrent+noumeraki;
if (noumeraki<drastMin) noumeraki=drastMax;
else if (noumeraki>drastMax) noumeraki=drastMin;
window.location.replace("dr"+noumeraki+".htm");
}

/**************************************************************/
/*		Question Type B (multiple choice) specific functions  */
/**************************************************************/

function showAnswers(totalradios) {
	var totalgroups = totalradios;
    var correct = 0;
    var wrong = 0;
    var blank = 0;
    for (var i=0;i<totalgroups;i++) {
        var yesChoice = eval("document.frmMain.answer" + i + "[0].checked");
        var noChoice  = eval("document.frmMain.answer" + i + "[1].checked");
        var yesAnswer = eval("document.frmMain.answer" + i + "[0].value");
        var noAnswer  = eval("document.frmMain.answer" + i + "[1].value");

        if (yesChoice == noChoice)
            blank++; // can't both be checked, thus must be both unchecked
        else {
            if ((yesChoice.toString() == yesAnswer) && (noChoice.toString() == noAnswer))
                correct++;
            else
                wrong++;
        }
    }
	showResultsQuestionTypeD(correct, totalgroups, totalgroups);
    //window.alert ("������: " + correct + ", �����: " + wrong + ", �����: " + blank);
}

function showResultsQuestionTypeD(lngCorrectAnswers, lngTotalAnswers, strCorrectAnswers) {
	var objDialogArgs = new Object();
	var lngRetVal;
	objDialogArgs.lngCorrectAnswers = lngCorrectAnswers;
	objDialogArgs.lngTotalAnswers = lngTotalAnswers;
	
	lngRetVal = window.showModalDialog("results.htm", objDialogArgs,'dialogWidth:520px; dialogHeight:220px; scroll: no; status:no');
	if (lngRetVal == 1) {					// Retry
		document.location.reload();	
	} else if (lngRetVal == 2) {	// Show Solution
		showSolutionQuestionTypeD(lngTotalAnswers);
	}
}

function showSolutionQuestionTypeD(strCorrectAnswers) {
	var i;
	
	document.frmMain.reset();
	
	for (i=0 ; i < strCorrectAnswers ; i++) {
		var yesChoice = eval("document.frmMain.answer" + i + "[0].checked");
    	var noChoice  = eval("document.frmMain.answer" + i + "[1].checked");
    	var yesAnswer = eval("document.frmMain.answer" + i + "[0].value");
    	var noAnswer  = eval("document.frmMain.answer" + i + "[1].value");
		
		//window.alert (yesAnswer + ", " + noAnswer);
	
		if (yesAnswer == 'true')
			eval("document.frmMain.answer" + i + "[0].checked = true");
		else
			eval("document.frmMain.answer" + i + "[1].checked = true");		
	}
}
